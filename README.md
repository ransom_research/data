### Country-Level Internet Paths ###

Each file shows the country-level path for accessing the given domain for a client in the country (in the filename).  For example, all paths in 'brazil_country_level_paths.txt' originate from a RIPE Atlas probe in Brazil.  It is not uncommon to see two different paths to the same destination domain --- this is because the probes are located in different parts of the origin country.

These country-level paths were generated from traceroute measurements run on the RIPE Atlas platform; all of these traceroutes are public measurements and can be searched at https://atlas.ripe.net/measurements/.
